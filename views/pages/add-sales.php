<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Sales";
$sidebarSection = "transaction";
$sidebarSubSection = "sales";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  
  ?>
  <link rel="stylesheet" href="<?= BASEASSETS; ?>vendor/toastr/toastr.min.css">
  <!--PLACE TO ADD YOUR CUSTOM CSS-->
  <style>
    .email-verify {
      background: green;
      color: #FFF;
      padding: 5px 10px;
      font-size: .875rem;
      line-height: 1.5;
      border-radius: .2rem;
      vertical-align: middle;
      /* display: none!important; */
    }
  </style>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Sales</h1>
          </div>

          <div class="row">

            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row justify-content-end">
                  <div class="mr-3">
                    <input type="text" class="form-control" name="email" id="customer_email"
                      placeholder="Enter Customer's Email">
                  </div>
                  <div>
                    <p class="email-verify" id="email_verify_success" style="display: none!important;">
                      <i class="fas fa-check fa-sm text-white mr-1"></i> Email Verified
                    </p>
                    <p class="email-verify bg-danger" id="email_verify_failed" style="display: none!important">
                      <i class="fas fa-times fa-sm text-white mr-1"></i> Email Not Verified
                    </p>
                    <a href="<?= BASEPAGES; ?>add-customer.php" class="btn btn-sm btn-warning shadow-sm d-inline-block"
                      id="add_customer_btn" style="display: none!important">
                      <i class="fas fas-users fa-sm text-white"></i> Add Customer
                    </a>
                    <button type="button" class="d-sm-inline-block btn btn-primary shadow-sm" name="check_email"
                      id="check_email">
                      <i class="fas fa-envelope fa-sm text-white"></i> Check Email
                    </button>
                  </div>
                </div>

                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
                  <button type="button" onclick="addProduct()"
                    class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                    <i class="fas fa-plus fa-sm text-white"></i> Add one more Product
                  </button>
                </div>
                <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-products">
                  <input type="hidden" name="customer_id" id="customer_id" value="">
                  <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                  <div class="card-body">
                    <div class="col-md-12">


                      <!--FORM GROUP-->
                      <div id="products_container">
                        <div class="row product_row" id="element_1">
                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="">Category</label>
                              <select id="category_1" class="form-control category_select">
                                <option disabled selected>Select Category</option>
                                <?php
                                    $categories = $di->get('database')->readData('category', ["id", "name"], "deleted=0");

                                    foreach ($categories as $category) {
                                        echo "<option value='{$category->id}'>{$category->name}</option>";
                                    }
                                    ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="">Products</label>
                              <select id="product_1" name="product_id[]" class="form-control product_select">
                                <option disabled selected>Select Product</option>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="">Selling Price</label>
                              <input type="text" id="selling_price_1" class="form-control" disabled>
                            </div>
                          </div>


                          <div class="col-md-1">
                            <div class="form-group">
                              <label for="">Quantity</label>
                              <input type="number" name="quantity[]" id="quantity_1"
                                class="form-control product_quantity" value="0">
                            </div>
                          </div>

                          <div class="col-md-1">
                            <div class="form-group">
                              <label for="">Discount</label>
                              <input type="number" name="discount[]" id="discount_1"
                                class="form-control product_discount" value="0">
                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="">Final Rate</label>
                              <input type="number" name="final_rate[]" id="final_rate_1" class="form-control final_rate"
                                value="0" readonly>
                            </div>
                          </div>



                          <div class="col-md-1">
                            <button onclick="deleteProduct(1)" type="button" class="btn btn-danger"
                              style="margin-top: 40%">
                              <i class="far fa-trash-alt"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                      <!--/FORM GROUP-->
                </form>

              </div>
              <!-- Card Footer -->
              <div class="card-footer d-flex justify-content-between">
                <div>
                  <input type="submit" class="btn btn-primary" name="final_submit"  id="btn_final_submit" value="submit">
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-4 col-form-label">Final Total</label>
                  <div class="col-sm-8">
                    <input type="text" readonly class="form-control" id="finalTotal" value="0">
                  </div>
                </div>
              </div>
              <!--/Card Footer  -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- End of Main Content -->
  <!-- Footer -->
  <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
  <!-- End of Footer -->
  </div>
  <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>

  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/transaction/add-sale-scripts.php");?>
</body>

</html>