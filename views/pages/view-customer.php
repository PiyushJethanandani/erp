<?php 


require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Customer view";
$sidebarSection = "view";
$sidebarSubSection = "";


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php require_once "../includes/head-section.php" ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "../includes/sidebar.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "../includes/navbar.php" ?>
        <!-- End of Topbar -->


<?php
$id = $_GET['id'];
$result = $di->get('customer')->getCustomerAndAddressById($id, PDO::FETCH_ASSOC);
// Util::dd($result);

// Util::dd($result);
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="container pt-5"> 
            <div class="row">
              <div class="col-md-12">
              <h4>Customer Details</h4>
              </div>
              <div class="col-md-6">
                <div class="row pt-4">
                  <div class="col-md-4">
                    <h5 class="text-dark">Name:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['first_name'] ?> <?= $result[0]['last_name'] ?></h5>
                  </div>
                </div>
                
                <div class="row pt-3">
                  <div class="col-md-4">
                    <h5 class="text-dark">Phone:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['phone_no'] ?></h5>
                  </div>
                </div>
                <div class="row pt-3">
                  <div class="col-md-4">
                    <h5 class="text-dark">Gender:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['gender'] ?></h5>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
               <div class="row pt-4">
                  <div class="col-md-4">
                    <h5 class="text-dark">GST No. :</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['gst_no'] ?></h5>
                  </div>
                </div>
                <div class="row pt-3">
                  <div class="col-md-4">
                    <h5 class="text-dark">Email Id:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['email_id'] ?></h5>
                  </div>
                </div>
                

              </div>
    
            </div>
            <div class="row pt-5">
                <div class="col-md-12">
                  <h4>Address</h4>
                </div>
                <div class="col-md-6">
                  <div class="row pt-4">
                      <div class="col-md-4">
                        <h5 class="text-dark">Block:</h5> 
                      </div>
                      <div class="col-md-8">
                        <h5 class="text-primary"><?= $result[0]['block_no'] ?></h5>
                      </div>
                    </div>
                    <div class="row pt-3">
                      <div class="col-md-4">
                        <h5 class="text-dark">Town:</h5> 
                      </div>
                      <div class="col-md-8">
                        <h5 class="text-primary"><?= $result[0]['town'] ?></h5>
                      </div>
                    </div>

                    <div class="row pt-3">
                      <div class="col-md-4">
                        <h5 class="text-dark">City:</h5> 
                      </div>
                      <div class="col-md-8">
                        <h5 class="text-primary"><?= $result[0]['city'] ?></h5>
                      </div>
                    </div>

                    <div class="row pt-3">
                      <div class="col-md-4">
                        <h5 class="text-dark">Country:</h5> 
                      </div>
                      <div class="col-md-8">
                        <h5 class="text-primary"><?= $result[0]['country'] ?></h5>
                      </div>
                    </div>
                </div>
                

              <div class="col-md-6">
               <div class="row pt-4">
                  <div class="col-md-4">
                    <h5 class="text-dark">Street :</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['street'] ?></h5>
                  </div>
                </div>
                <div class="row pt-3">
                  <div class="col-md-4">
                    <h5 class="text-dark">Pincode:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['pincode'] ?></h5>
                  </div>
                </div>
                <div class="row pt-3">
                  <div class="col-md-4">
                    <h5 class="text-dark">State:</h5> 
                  </div>
                  <div class="col-md-8">
                    <h5 class="text-primary"><?= $result[0]['state'] ?></h5>
                  </div>
                </div>
                </div>
           </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once "../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once "../includes/scroll-to-top.php" ?>;

  <!-- Logout Modal-->
  

  <!-- Core Scripts -->
  <?php require_once "../includes/core-scripts.php" ?>;

 

</body>

</html>
