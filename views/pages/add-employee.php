<?php
require_once __DIR__.'/../../helper/init.php';
$pageTitle = "Easy ERP | Add Employee";
$sidebarSection = "employee";
$sidebarSubSection = "add"; 
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Employee</h1>
            <a href="<?= BASEPAGES; ?>manage-employee.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Employee
            </a>
          </div>

          <div class="row">
            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
              </div>
              <div class="card-body">
                <div class="col-md-12">
                  <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-employee">
                    <input type="hidden" name="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                    <!-- FORM GROUP -->
                    <div class="form-group">
                      <div class="row">
                      <div class="col-md-6 col-sm-6">
                      <label for="first_name">Employee First Name</label>
                      <input type="text" name="first_name" id="first_name" class="form-control <?= $errors!='' && $errors->has('first_name')? 'error': '';?>"
                      placeholder="Enter Your First Name"/>
                      
                      <?php
                        if($errors!="" && $errors->has('first_name'))
                        {
                          echo "<span class='error'>{$errors->first('first_name')}</span>";
                        }
                      ?>
                      <br>


<label for="email_id">Employee Email ID</label>
                      <input type="email" name="email_id" id="email_id" class="form-control <?= $errors!='' && $errors->has('email_id')? 'error': '';?>"
                      placeholder="Enter Your Email Id"/>
                      
                      <?php
                        if($errors!="" && $errors->has('email_id'))
                        {
                          echo "<span class='error'>{$errors->first('email_id')}</span>";
                        }
                      ?>
                      <br>

                      <label for="gender">Employee Gender</label>
                      <select name="gender" id="gender" class="form-control">
                      <option></option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      </select>
                      <?php
                        if($errors!="" && $errors->has('gender'))
                        {
                          echo "<span class='error'>{$errors->first('gender')}</span>";
                        }
                      ?>
                      <br>

                      </div>
                      <div class="col-md-6">
                      <label for="last_name">Employee Last Name</label>
                      <input type="text" name="last_name" id="last_name" class="form-control <?= $errors!='' && $errors->has('last_name')? 'error': '';?>"
                      placeholder="Enter Your Last Name"/>
                     
                      <?php
                        if($errors!="" && $errors->has('last_name'))
                        {
                          echo "<span class='error'>{$errors->first('last_name')}</span>";
                        }
                      ?>
                      <br>

<label for="phone_no">Employee Phone Number</label>
                      <input type="number" name="phone_no" id="phone_no" class="form-control <?= $errors!='' && $errors->has('phone_no')? 'error': '';?>"
                      placeholder="Enter Your Phone Number"/>
                      <?php
                        if($errors!="" && $errors->has('phone_no'))
                        {
                          echo "<span class='error'>{$errors->first('phone_no')}</span>";
                        }
                      ?>
                      <br>


                      </div>
                      </div>

                      <label for="address">Address</label>
                      <div class="row">
                      <div class="col-md-6">
                      <input type="text" name="block_no" id="block_no" class="form-control <?= $errors!='' && $errors->has('block_no')? 'error': '';?>"
                      placeholder="Enter Block No."/>
                     
                      <?php
                        if($errors!="" && $errors->has('block_no'))
                        {
                          echo "<span class='error'>{$errors->first('block_no')}</span>";
                        }
                      ?>
                      </div>
                      <div class="col-md-6">
                      <input type="text" name="street" id="street" class="form-control <?= $errors!='' && $errors->has('street')? 'error': '';?>"
                      placeholder="Enter street"/>
                     
                      <?php
                        if($errors!="" && $errors->has('street'))
                        {
                          echo "<span class='error'>{$errors->first('street')}</span>";
                        }
                      ?>

                      
                      </div>
                      </div>

                      <div class="row pt-3">
                            <div class="col-md-4">
                            <input type="text" name="city" id="city" class="form-control <?= $errors!='' && $errors->has('city')? 'error': '';?>"
                            placeholder="Enter City"/>
                          
                            <?php
                              if($errors!="" && $errors->has('city'))
                              {
                                echo "<span class='error'>{$errors->first('city')}</span>";
                              }
                            ?>
                            </div> 

                            <div class="col-md-4">
                            <input type="text" name="state" id="state" class="form-control <?= $errors!='' && $errors->has('state')? 'error': '';?>"
                            placeholder="Enter State"/>
                          
                            <?php
                              if($errors!="" && $errors->has('state'))
                              {
                                echo "<span class='error'>{$errors->first('state')}</span>";
                              }
                            ?>
                            </div>

                            <div class="col-md-4">
                            <input type="text" name="town" id="town" class="form-control <?= $errors!='' && $errors->has('town')? 'error': '';?>"
                            placeholder="Enter Town"/>
                          
                            <?php
                              if($errors!="" && $errors->has('town'))
                              {
                                echo "<span class='error'>{$errors->first('town')}</span>";
                              }
                            ?>
                            </div> 
                       </div>

                       <div class="row pt-3 pb-3">
                            <div class="col-md-6">
                            <input type="number" name="pincode" id="pincode" class="form-control <?= $errors!='' && $errors->has('pincode')? 'error': '';?>"
                            placeholder="Enter Pin Code"/>
                          
                            <?php
                              if($errors!="" && $errors->has('pincode'))
                              {
                                echo "<span class='error'>{$errors->first('pincode')}</span>";
                              }
                            ?>
                            </div> 

                            <div class="col-md-6">
                            <input type="text" name="country" id="country" class="form-control <?= $errors!='' && $errors->has('country')? 'error': '';?>"
                            placeholder="Enter Country"/>
                          
                            <?php
                              if($errors!="" && $errors->has('country'))
                              {
                                echo "<span class='error'>{$errors->first('country')}</span>";
                              }
                            ?>
                            </div> 
                       </div>
                    <!--/FORM GROUP-->
                    <button type="submit" class="btn btn-primary" name="add_employee" value="addEmployee"><i class="fa fa-check"></i> Submit</button>
                  </form>
                </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/employee/add-employee-scripts.php");?>
</body>

</html>