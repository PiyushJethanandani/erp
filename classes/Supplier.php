<?php

require_once __DIR__."/../helper/requirements.php";

class Supplier{
    private $table = "suppliers";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
                
            ],

            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
                
            ],
            'gst_no' => [
                'required' => true
               
               
            ],
            'phone_no' => [
                'required' => true,
                

            ],
            'email_id' => [
                'required' => true,
                'email' => true
                
            ],
            'company_name'=>[
                'required'=>true
            ]
         
        ]);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addSupplier($data)
    {
        // Util::dd($data);
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name' => $data['first_name'], 
                    'last_name' => $data['last_name'], 
                    'gst_no' => $data['gst_no'], 
                    'email_id' => $data['email_id'], 
                    'phone_no' => $data['phone_no'], 
                    'company_name' => $data['company_name']
                ];

                $address_to_be_inserted = [
                    'block_no' => $data['block_no'],
                    'street' => $data['street'],
                    'state' => $data['state'],
                    'country' => $data['country'],
                    'town' => $data['town'],
                    'pincode' => $data['pincode'],
                    'city' => $data['city']
                ];
                $supplier_id = $this->database->insert($this->table, $data_to_be_inserted);
                $address_id = $this->database->insert("address", $address_to_be_inserted);

                $address_supplier_insert = [
                    'supplier_id' => $supplier_id,
                    'address_id' => $address_id
                ]; 

                $address_supplier_id = $this->database->insert('address_supplier', $address_supplier_insert);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                // Util::dd($e);
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

public function getJSONDataForDataTable($draw, $searchParameter, $orderBy, $start, $length){
    $columns = ["full_name", "gst_no", "phone_no", "email_id", "company_name", "full_address"];
    $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} WHERE deleted = 0";
    $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table} WHERE deleted = 0";
    $query = "SELECT *, CONCAT(first_name,' ', last_name) as full_name, CONCAT(block_no,' ', street,' ', town, ' ', city, ' ', pincode, ' ', state, ' ', country) as full_address  FROM  address, address_supplier, {$this->table} WHERE address_supplier.address_id = address.id AND address_supplier.supplier_id = {$this->table}.id AND address.deleted = 0 AND {$this->table}.deleted = 0";
     

    if($searchParameter != null){
        $query .= " AND CONCAT(first_name, last_name, gst_no,  phone_no, email_id, company_name, CONCAT(block_no,' ', street,' ', town, ' ', city, ' ', pincode, ' ', state, ' ', country)) LIKE '%{$searchParameter}%'";
        // $filteredRowCountQuery .= " AND CONCAT(first_name, last_name)  LIKE '%{$searchParameter}%'";
        // die($filteredRowCountQuery);
    }
    if ($orderBy != null) {
        $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        // die(print_r($columns[$orderBy]));
        // die($query);
    }else {
        $query .= " ORDER BY {$columns[0]} ASC";
    }

    if ($length != -1) {
        $query .= " LIMIT {$start}, {$length}";
    }

    $totalRowCountResult = $this->database->raw($totalRowCountQuery);
    $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

    $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
    $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count : 0;

    $filteredData = $this->database->raw($query);
    // die(print_r($filteredData));
    $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
    $data = [];
    for ($i=0; $i < $numberOfRowsToDisplay; $i++) { 
        $subarray = [];
        $subarray[] = $filteredData[$i]->full_name;
        $subarray[] = $filteredData[$i]->gst_no;
        $subarray[] = $filteredData[$i]->phone_no;
        $subarray[] = $filteredData[$i]->email_id;
        $subarray[] = $filteredData[$i]->company_name;
        $subarray[] = $filteredData[$i]->full_address;
        $subarray[] = 
        <<<BUTTONS
        <button class="view btn btn-outline-warning m-1" id="{$filteredData[$i]->id}" data-toggle = "modal" data-target = "#viewModal" name="view_customer">
            <i class="fas fa-eye"></i>
        </button>
        <button class="edit btn btn-outline-primary m-1" id="{$filteredData[$i]->id}">
            <i class="fas fa-pencil-alt"></i>
        </button>
        <button class="delete btn btn-outline-danger m-1" id="{$filteredData[$i]->id}" data-toggle = "modal" data-target = "#deleteModal">
            <i class="fas fa-trash"></i>
        </button>
BUTTONS;

        $data[] = $subarray;
    }

    $output = [
        "draw" => $draw,
        "recordsTotal" => $numberOfTotalRows,
        "recordsFiltered" => $numberOfFilteredRows,
        "data" => $data
    ];

    echo json_encode($output);
}

public function getSupplierById($customerId, $mode=PDO::FETCH_OBJ)
{
    $query = "SELECT * FROM {$this->table} WHERE deleted = 0 AND id = {$customerId}";
    $result = $this->database->raw($query, $mode);
    return $result;
}


public function getSupplierAndAddressById($supplier_id, $mode=PDO::FETCH_OBJ)
{
    $query = "SELECT *, CONCAT(first_name,' ', last_name) as full_name, CONCAT(block_no,' ', street,' ', town, ' ', city, ' ', pincode, ' ', state, ' ', country) as full_address  FROM {$this->table}, address,address_supplier WHERE address_supplier.address_id = address.id AND address_supplier.supplier_id = {$this->table}.id AND {$this->table}.id = $supplier_id AND address.deleted = 0 AND {$this->table}.deleted = 0";
    
    $result = $this->database->raw($query, $mode);
    return $result;
}
public function update($data, $id){
    $validation = $this->validateData($data);
    if(!$validation->fails())
    {
        //Validation was successful
        try
        {
            //Begin Transaction
            $this->database->beginTransaction();
            $data_to_be_updated = [
                'first_name' => $data['first_name'], 
                'last_name' => $data['last_name'], 
                'gst_no' => $data['gst_no'], 
                'email_id' => $data['email_id'], 
                'phone_no' => $data['phone_no'], 
                'company_name' => $data['company_name']
            ];
            
            $address_to_be_updated = [
                'block_no' => $data['block_no'],
                'street' => $data['street'],
                'state' => $data['state'],
                'country' => $data['country'],
                'town' => $data['town'],
                'pincode' => $data['pincode'],
                'city' => $data['city']
            ];

            // die(var_dump($filteredData));
            $this->database->update($this->table, $data_to_be_updated, "id = {$id}");
            $query = "SELECT address_id FROM address_supplier WHERE supplier_id = {$id}";
            $this->database->update('address', $address_to_be_updated, "id = {$this->database->raw($query, PDO::FETCH_ASSOC)[0]['address_id']}");
            
            $this->database->commit();
            return EDIT_SUCCESS;
        }
        catch(Exception $e)
        {
            $this->database->rollback();
            // Util::dd($e);
            return EDIT_ERROR;
        }
    }
    else
    {
        //Validation Failed!
        return VALIDATION_ERROR;
    }
}

public function delete($id)
    {
        try{
            $this->database->beginTransaction();
            $this->database->delete($this->table, "id={$id}");
            $query = "SELECT address_id FROM address_supplier WHERE supplier_id = {$id}";
            // Util::dd($this->database->raw($query, PDO::FETCH_ASSOC)[0]['address_id']);
            $this->database->delete("address", "id = {$this->database->raw($query, PDO::FETCH_ASSOC)[0]['address_id']}"); 
            $this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e){
            $this->database->rollback();
            return DELETE_ERROR;
        }
    }

    public function selectedMale($data){
        if($data == 'Male'){
            return 'selected';
        }
    }

    public function selectedFemale($data){
        if($data == 'Female'){
            return 'selected';
        }
    }

}



