<?php

require_once __DIR__."/../helper/requirements.php";

class Sales{
    private $table = "sales";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
      $validation = true;
      return $validation;
    }
    public function addInvoice($data){
        $data_to_be_inserted = ['customer_id' => $data];
        $invoice_id = $this->database->insert('invoice', $data_to_be_inserted);
        return $invoice_id;
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addSales($data, $invoice_id)
    {
        $validation = $this->validateData($data);
        // if(!$validation->fails())
        // {
            //Validation was successful 
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                

                $data_to_be_inserted = [];
                $data_to_be_inserted['invoice_id'] = $invoice_id;
                
                
                for($i=0; $i<count($data['product_id']); $i++){
                    $data_to_be_inserted['product_id'] = $data['product_id'][$i];
                    $data_to_be_inserted['quantity'] = $data['quantity'][$i];
                    $data_to_be_inserted['discount'] = $data['discount'][$i];
                    // print_r($data_to_be_inserted);

                    $this->database->insert('Sales', $data_to_be_inserted); 
                }
                // Util::dd("hello");
                $this->database->commit();
                return $invoice_id;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }


        public function getCustomerDetailsByInvoiceId($invoice_id){
            $query = "SELECT first_name,last_name,gst_no,phone_no,email_id,gender FROM invoice,customers WHERE invoice.customer_id = customers.id AND invoice.id = $invoice_id";

            $result = $this->database->raw($query);
            return $result;
        }
        // else
        // {
        //     //Validation Failed!
        //     return VALIDATION_ERROR;
        // }

        public function getSalesDetailsByInvoiceId($invoice_id){
            $query = "SELECT t1.name, t1.specification, t1.hsn_code, t1.eoq_level, t1.danger_level, (t1.quantity) as product_total_quantity, (t2.quantity) as sold_quantity, t2.discount, t3.selling_rate FROM products t1, {$this->table} t2 INNER JOIN(SELECT product_id, selling_rate, MAX(created_at) as ca FROM products_selling_rate WHERE created_at <= CURRENT_TIMESTAMP GROUP BY product_id) t3 ON t3.product_id = t2.product_id WHERE t2.product_id = t1.id AND t2.invoice_id = {$invoice_id}";
            $result = $this->database->raw($query);
            return $result;
        }

    

}