var TableDataTables = function(){
    var handleCategoryTable = function(){
        var manageCustomerTable = $("#manage-customer-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageCustomerTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_customer"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [0, -1]
            }],
        });

        manageCustomerTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            $("#category_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
                dataType: "json",
                success: function(data){
                    $("#category_name").val(data[0].name);
                }
            });
        });

        manageCustomerTable.on('click', '.delete', function(){
            id = $(this).attr('id');
            $("#record_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
                dataType: "json"
            });
        });


        manageCustomerTable.on('click', '.view', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/view-customer.php/?id="+id;
            
        });

        manageCustomerTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/edit-customer.php/?id="+id;
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "customer_id": id,
                    "fetch": "customers"
                },
                dataType: "json"
                // success: function(data){
                //     $("#customer_name").val(data[0].name);
                // }
            });
            
        });
    }

    return{
        init: function(){
            handleCategoryTable();
        }
    }
}();


jQuery(document).ready(function(){
    TableDataTables.init();
})