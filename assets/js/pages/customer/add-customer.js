$(function(){
    $("#add-customer").validate({
        'rules': {
            'first_name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'last_name':{
                required:true,
                minlength:2,
                maxlength:255
            },
            'gst_no':{
                required:true
            },
            'phone_no':{
                required:true
            },
            'email_id':{
                required:true,
                minlength:2,
                maxlength:255
            },
            'gender':{
                required:true
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});