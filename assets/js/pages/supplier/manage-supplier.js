var TableDataTables = function(){
    var handleCategoryTable = function(){
        var manageSupplierTable = $("#manage-supplier-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageSupplierTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_supplier"
                    // "fetch": 'supplier'
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [0, -1]
            }],
        });

        manageSupplierTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            $("#category_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "supplier_id": id,
                    "fetch": "category"
                },
                dataType: "json"
                // success: function(data){
                //     $("#supp_name").val(data[0].name);
                // }
            });
        });

        manageSupplierTable.on('click', '.delete', function(){
            id = $(this).attr('id');
            $("#record_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
                dataType: "json"
            });
        });


        manageSupplierTable.on('click', '.view', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/view-supplier.php/?id="+id;
            
        });

        manageSupplierTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/edit-supplier.php/?id="+id;
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "supplier_id": id,
                    "fetch": "suppliers"
                },
                dataType: "json"
                // success: function(data){
                //     $("#customer_name").val(data[0].name);
                // }
            });
            
        });
    }

    return{
        init: function(){
            handleCategoryTable();
        }
    }
}();


jQuery(document).ready(function(){
    TableDataTables.init();
})