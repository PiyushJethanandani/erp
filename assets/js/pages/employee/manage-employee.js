var TableDataTables = function(){
    var handleCategoryTable = function(){
        var manageEmployeeTable = $("#manage-employee-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageEmployeeTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_employee"
                    // "fetch": 'supplier'
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [0, -1]
            }],
        });

        manageEmployeeTable.on('click', '.delete', function(){
            id = $(this).attr('id');
            $("#record_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "employee_id": id,
                    "fetch": "employee"
                },
                dataType: "json"
            });
        });


        manageEmployeeTable.on('click', '.view', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/view-employee.php/?id="+id;
            
        });

        manageEmployeeTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + "/views/pages/edit-employee.php/?id="+id;
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "employee_id": id,
                    "fetch": "employees"
                },
                dataType: "json"
                // success: function(data){
                //     $("#customer_name").val(data[0].name);
                // }
            });
            
        });
    }

    return{
        init: function(){
            handleCategoryTable();
        }
    }
}();


jQuery(document).ready(function(){
    TableDataTables.init();
})