var id = 2;
var baseURL = window.location.origin;
var filePath = "/helper/routing.php";
var submitCustomerId;
function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1){
        $("#element_"+delete_id).remove();
        $("#finalTotal").val(addFinalRate());
    }
}

// $(function(){
//   $("#add-products").validate({
      
//     'rules': {
//         'customer_id':{
//             required: true
//         },
//         'quantity[]':{
//           allrequired: true
//         }
        
//     },
  
//     submitHandler: function(form) {
//         // do other things for a valid form
//         alert("submit!");
//         form.submit();
  
//     }
//   });
// });

function addProduct(){
    $("#products_container").append(`<div class="row product_row" id="element_${id}">
    <div class="col-md-2">
        <div class="form-group">
          <label for="">Category</label>
          <select id="category_${id}" class="form-control category_select">
            <option disabled selected>Select Category</option>
            
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Products</label>
          <select id="product_${id}" name="product_id[]" class="form-control product_select">
            <option disabled selected>Select PRoduct</option>
          </select>
        </div>
      </div>

      <div class="col-md-2">
        <div class="form-group">
          <label for="">Selling Price</label>
          <input type="text" id="selling_price_${id}" class="form_control" disabled>
        </div>
      </div>

      <div class="col-md-1">
          <div class="form-group">
          <label for="">Quantity</label>
          <input type="number" name="quantity[]" id="quantity_${id}" class="form-control product_quantity" value="0">
        </div>
      </div>

      <div class="col-md-1">
        <div class="form-group">
          <label for="">Discount</label>
          <input type="number" name="discount[]" id="discount_${id}" class="form-control product_discount" value="0">
        </div>
      </div>

      <div class="col-md-2">
        <div class="form-group">
          <label for="">Final Rate</label>
          <input type="number" name="final_rate[]" id="final_rate_${id}" class="form-control final_rate" value="0" readonly>
        </div>
      </div>

      <div class="col-md-1">
        <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top: 40%">
          <i class="far fa-trash-alt"></i>
        </button>
      </div>
    </div>`);

    $.ajax({
      url: baseURL+filePath,
      method: 'POST',
      data:{
        getCategories: true
      },
      dataType: 'json',
      success: function(categories){
        categories.forEach(function (category){
          $("#category_"+id).append(
            
            `<option value='${category.id}'>${category.name}</option>`
          );
          
        });
        id++;
      }
    });
}


$("#products_container").on('change', '.category_select', function(){
  var element_id = $(this).attr('id').split("_")[1];
  var category_id = this.value;
  $.ajax({
    url: baseURL+filePath,
    method: 'POST',
    data:{
      getProductsByCategoryID: true,
      categoryID: category_id
    },
    dataType: 'json',
    success: function(products){
      // console.log(products);
      $("#product_"+element_id).empty();
      $("#product_"+element_id).append("<option disabled selected>Select Prodcut</option>");
      products.forEach(function (product){
        $("#product_"+element_id).append(
          `<option value='${product.id}'>${product.name}</option>`        
        );
      });
    }
  });
});

$("#products_container").on('change', '.product_select', function(){
  // console.log("Hello");
  var element_id = $(this).attr('id').split("_")[1];
  var product_id = this.value;
  
  // console.log(element_id);
  // console.log(product_id);
  $.ajax({
    url: baseURL+filePath,
    method: 'POST',
    data:{
      getSellingPriceByProductID: true,
      productID: product_id
    },
    dataType: 'json',
    success: function(sellingRates){
      // console.log(product_selling_rate);
      $("#selling_price_"+element_id).empty();
      $("#final_rate_"+element_id).val("");
      // $("#selling_price_"+element_id).append("");
      sellingRates.forEach(function (sellingRate){
        // console.log(sellingRate);
        $("#selling_price_"+element_id).val(sellingRate.selling_rate);
        $("#finalTotal").val(addFinalRate());
      });
    }
  });
});

$("#products_container").on('change', '.product_quantity, .product_discount', function(){
  // console.log("Hello");
  var element_id = $(this).attr('id').split("_")[1];
  var product_id = $("#product_"+element_id).children("option:selected").val();
  console.log(product_id);
  
  
  
  // console.log(element_id);
  // console.log(product_id);
  $.ajax({
    url: baseURL+filePath,
    method: 'POST',
    data:{
      getSellingPriceByProductID: true,
      productID: product_id
    },
    dataType: 'json',
    success: function(sellingRates){
      // console.log(product_selling_rate);
      $("#final_rate_"+element_id).val("");
      
      // $("#selling_price_"+element_id).append("");
      // var sum;
      
      sellingRates.forEach(function (sellingRate){
        console.log(sellingRate);
        discount_rate = ((sellingRate.selling_rate * $("#quantity_"+element_id).val())/100) * $("#discount_"+element_id).val();
        final_rate =  (sellingRate.selling_rate * $("#quantity_"+element_id).val()) - discount_rate;
        $("#final_rate_"+element_id).val(final_rate);
        $("#finalTotal").val(addFinalRate());
      });

      // for(final_rate = 0; final_rate != null ; final_rate++){
      //   var sum;
      //   sum += final_rate.val();
      //   $("#finalTotal").val(sum);
      // }     
    }
  });
  

  
  
});



// $("#products_container").on('change', '.product_row', function(){
  
//   $("#finalTotal").val(addFinalRate());
// });


function addFinalRate(){
  var finalRate = document.querySelectorAll('.final_rate');
  console.log(finalRate);
  var sum = 0;
  finalRate.forEach(function(rate){
    console.log(rate);
    sum += parseInt(rate.value);
  });
  return sum;
}


$("#check_email").on('click', function(){
  
  var email_verify = document.getElementById('customer_email').value;
  $.ajax({
    url: baseURL+filePath,
    method: 'POST',
    data : {
      getCustomerIDByEmail : true,
      email : email_verify
    },
    dataType : 'json',
    success: function(customerId){
      if(customerId){
        submitCustomerId = true;
        $("#email_verify_success").css("display", "block");
        $("#customer_id").val(customerId[0]['id']);
        $("#email_verify_failed").css("display", "none");
        $("#add_customer_btn").attr("style", "display: none!important");
      }else{
        // submitCustomerId = false;
        $("#email_verify_failed").css("display", "block");
        $("#add_customer_btn").css("display", "block");
        
        $("#email_verify_success").css("display", "none");
        $("#customer_id").val("");
        
      }
    }
  })
});


// $(function(){
  
//   $("#add-products").validate({
//     'rules': {
//         'customer_id':{
//             required: true
//         },
//         'quantity[]': {
//             allrequired: true
//         }
        
//     },
//     'messages':{
//       'quantity[]': "please select at least one product"
//     },
//     submitHandler: function(form) {
//         // do other things for a valid form
//         form.submit();

//     }
//   });
// });

$("#btn_final_submit").on('click', function(e){

  if(submitCustomerId){
    removeEmptyQuantityProduct();
    $("#btn_final_submit").submit();
  }
    // $("#btn_final_submit").submit();
  else{
    e.preventDefault();
    console.log("hello");
    $("#email_verify_failed").css("display", "block");
    $("#add_customer_btn").css("display", "block");   
    toastr.error("please verify email", "Failed");
  }
});

function removeEmptyQuantityProduct(){
  var quantity_array = document.querySelectorAll('.product_quantity');
  quantity_array.forEach(function(quantity){
     if(parseInt(quantity.value) == 0){
       $("#element_"+quantity.id.split('_')[1]).remove();
    }
  });
}




function checkEmptyQuantity(){
  var quantity_array = document.querySelectorAll('.product_quantity');
  var elements = document.getElementsByClassName("product_row");
  quantity_array.forEach(function(quantity){
  if(parseInt(quantity.value) == 0){
        toastr.error("input atleast one quantity", "Failed");
    }
  });

}

